open cypress/e2e/spec.cy.js

fill credentials (email/password) for https://awsevents.onpeakapps.com/e/awsreinvent2022

use ci/cd schedule from gitlab, use "0 * * * *" as expression (gitlab scheduled pipelines  cannot run more frequently than once per 60 minutes.)

wait for email that pipeline was fixed :)
