describe('empty spec', () => {
  it('passes', () => {
    cy.visit('https://awsevents.onpeakapps.com/e/awsreinvent2022')    
    cy.get('div.loginHint a').click()
    cy.get('input[type="email"]').type('') // type email here
    cy.get('input[type="password"]').type('')  // type password here
    cy.get('div.loginControls > button').click()
    cy.get('div.linkToRes > div.linkCopy').click()
    cy.get('div.dataSelectButton > div.dataSelectDownArrow').eq(1).click()
    cy.get('div.dateChangeColumn').contains('Sun, Nov 27, 2022').parent().parent().should('not.have.class','unavailableRow')
  })
})
